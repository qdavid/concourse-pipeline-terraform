variable "project_name" {
    default = "noname"
}

variable "phase" {
    default = "dev"
}

variable "image" {
    default = "ubuntu18.04_server"
}

variable "kp_public_key" {
    default = ""
}

variable "dns_ip" {
    type = list(string)
    default = ["10.156.32.33", "10.154.59.104"]
}

variable "admin_cidr" {
    default = "172.18.0.0/24"
}

variable "ssh_keypair" {
    default = "cycloid"
}

locals {
    platform_name = "${var.project_name}-${var.phase}"
}