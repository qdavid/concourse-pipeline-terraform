provider "openstack" {
    cacert_file = "rie-ca-bundle.crt"
    auth_url = "${var.auth_url}"
    user_name = "${var.user_name}"
    password = "${var.password}"
    tenant_name = "${var.tenant_name}"
}
