resource "openstack_networking_network_v2" "network_adm" {
  name           = "${local.platform_name}_network_adm"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "network_adm_subnet" {
  name       = "${local.platform_name}_network_adm_subnet"
  network_id = openstack_networking_network_v2.network_adm.id
  cidr       = var.admin_cidr
  ip_version = 4
  dns_nameservers   = var.dns_ip
}

resource "openstack_networking_subnet_route_v2" "network_adm_subnet_route_1" {
  subnet_id        = openstack_networking_subnet_v2.network_adm_subnet.id
  destination_cidr = "100.65.1.1/32"
  next_hop         = "172.18.0.1"
}

resource "openstack_networking_subnet_route_v2" "network_adm_subnet_route_2" {
  subnet_id        = openstack_networking_subnet_v2.network_adm_subnet.id
  destination_cidr = "100.67.224.19/32"
  next_hop         = "172.18.0.1"
}

resource "openstack_networking_subnet_route_v2" "network_adm_subnet_route_3" {
  subnet_id        = openstack_networking_subnet_v2.network_adm_subnet.id
  destination_cidr = "100.67.226.0/23"
  next_hop         = "172.18.0.1"
}

resource "openstack_networking_router_v2" "router_adm" {
  name                = "${local.platform_name}_router_admin"
  admin_state_up      = true
  external_network_id = "962d5c01-6597-4606-949c-da53222a47e0"
}

resource "openstack_networking_router_interface_v2" "router_adm_interface_network_adm" {
  router_id = openstack_networking_router_v2.router_adm.id
  subnet_id = openstack_networking_subnet_v2.network_adm_subnet.id
}