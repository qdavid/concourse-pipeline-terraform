resource "openstack_networking_network_v2" "network_pub" {
  name           = "${local.platform_name}_network_pub"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "network_pub_subnet" {
  name       = "${local.platform_name}_network_pub_subnet"
  network_id = openstack_networking_network_v2.network_pub.id
  cidr       = "172.16.0.0/24"
  ip_version = 4
}

resource "openstack_networking_router_v2" "router_pub" {
  name                = "${local.platform_name}_router_pub"
  admin_state_up      = true
  external_network_id = "0c8487b7-0975-49a7-aa90-51ea3870d305"
}