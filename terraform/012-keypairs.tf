data "openstack_compute_keypair_v2" "kp" {
  name = "cycloid"
  public_key = "${vars.kp_public_key}"
}
