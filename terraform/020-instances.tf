resource "openstack_compute_instance_v2" "appli01" {
  name            = "${local.platform_name}-appli01"
  image_name      = var.image
  flavor_name     = "CO1.4"
  key_pair        = var.ssh_keypair
  security_groups = ["default"]

  metadata = {
    platform_name = local.platform_name
    ansible_group = "data"
    group = ["data", tag_env_${vars.phase}, tag_project_${vars.project_name}]

  }

  network {
    name = "${local.platform_name}_network_adm"
  }

  network {
    name = "${local.platform_name}_network_pub"
  }
}

resource "openstack_networking_floatingip_v2" "fip_admin" {
  pool = "FIP_ADMINISTRATION_DGFIP"
}

resource "openstack_compute_floatingip_associate_v2" "fip_admin" {
  floating_ip = openstack_networking_floatingip_v2.fip_admin.address
  instance_id = openstack_compute_instance_v2.appli01.id
  fixed_ip    = openstack_compute_instance_v2.appli01.network.0.fixed_ip_v4
}
